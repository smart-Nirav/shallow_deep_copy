# Reference variable:

In a simple word we can say value stored with memory address.We can access or oprate the value with address. There are several data types available in javascript like _Array, Object, Interface_. This are also calles nonprimitive data types.

# Shallow Copy:

A shallow copy of an object is a copy whose properties share the same references (point to the same underlying values) as those of the source object from which the copy was made.

When you change either the source or the copy, you may also cause the other object to change too.

For shallow copies, it's important to understand that selectively changing the value of a shared property of an existing element in an object is different from assigning a completely new value to an existing element.

# Deep Copy:

A deep copy of an object is a copy whose properties **DO NOT** share the same references (point to the same underlying values) as those of the source object from which the copy was made.

When you change either the source or the copy, you can be assured you're not causing the other object to change too.

That is, you won't unintentionally be causing changes to the source or copy that you don't expect.

# Destructring:

Data can be extracted from arrays, objects, nested objects and assigning to variables.

EX:
let a, b, rest;
[a, b] = [10, 20];

[a, b, ...rest] = [10, 20, 30, 40, 50];

console.log(rest);
// Expected output: Array [30, 40, 50]
