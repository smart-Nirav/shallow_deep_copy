let obj = {}
let user = obj;
console.log(user)
obj = {
    name: "Nirav",
    address: {
        city: "Bhuj"
    }
}

// user.name = "Rahul"
console.log(obj)

//First way of shallow copy - with Object assign
// let user = Object.assign({}, obj);

//Second way of shallow copy - with destructuring
// let user = { ...obj }

//Deep copy - First we convert object into string with JSON.stringify and then again convert into object with JSON.parse
let user = JSON.parse(JSON.stringify(obj));

user.name = "Rahul";
user.address.city = "Gandhinagar"

console.log("Object:", obj);
console.log("User:", user)
